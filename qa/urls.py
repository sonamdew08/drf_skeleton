from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns
from qa import views


urlpatterns = [
    path('questions/', views.QuestionList.as_view()),
    path('questions/<int:pk>', views.QuestionDetail.as_view(), name='question'),
    path('questions/<int:pk>/answers', views.AnswerList.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
