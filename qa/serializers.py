from rest_framework import serializers
from qa.models import Question, Answer


class AnswerSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Answer
        fields = ('user', 'question', 'content', 'votes')

class QuestionSerializer(serializers.ModelSerializer):

    answers = AnswerSerializer(many=True, read_only=True)
    class Meta:
        model = Question
        fields = ('id', 'title', 'description', 'answers')

class QuestionlinkSerializer(serializers.ModelSerializer):
    question = serializers.HyperlinkedRelatedField(
        read_only=True,
        view_name='question'
    )
    class Meta:
        model = Answer
        fields = ('user', 'question', 'content', 'votes')