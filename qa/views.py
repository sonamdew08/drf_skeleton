from qa.models import Question, Answer
from qa.serializers import QuestionSerializer, QuestionlinkSerializer
from rest_framework import generics
from qa.permissions import IsOwnerOrReadOnly


class QuestionList(generics.ListCreateAPIView):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer
    permission_classes = (IsOwnerOrReadOnly,)
    def perform_create(self, serializer):
        serializer.save(user = self.request.user)
        
   
class QuestionDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer
    permission_classes = (IsOwnerOrReadOnly,)

  
class AnswerList(generics.ListCreateAPIView):
    model = Answer
    def get_queryset(self):
        question = self.kwargs['pk']
        queryset = Answer.objects.filter(question = question)
        return queryset
    serializer_class = QuestionlinkSerializer
    
            
    
